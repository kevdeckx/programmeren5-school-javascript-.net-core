function controller() {
    let table = document.querySelector('table.spreadsheet');
    table.addEventListener('click',
        function(event) {
            // alert (event.target.tagName)
            if (event.target.tagName == 'TD') {
                event.target.style.backgroundColor = 'yellow';
            }
            else if (event.target.tagName == 'TH') {
                if (event.target.getAttribute('scope') == 'col') {
                    event.target.style.backgroundColor = 'green';
                }
                else {
                    event.target.style.backgroundColor = 'red';
                }
            }
        }, false);
}

window.onload = function () {
    controller();
}