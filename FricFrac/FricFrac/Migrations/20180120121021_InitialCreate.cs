﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FricFrac.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "country",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(maxLength: 2, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "eventcategory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 120, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_eventcategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "eventtopic",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 120, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_eventtopic", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "role",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "person",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Address1 = table.Column<string>(maxLength: 255, nullable: true),
                    Address2 = table.Column<string>(maxLength: 255, nullable: true),
                    Birthday = table.Column<DateTime>(type: "datetime", nullable: true),
                    City = table.Column<string>(maxLength: 80, nullable: true),
                    CountryId = table.Column<int>(type: "int(11)", nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 120, nullable: false),
                    Password = table.Column<string>(maxLength: 255, nullable: true),
                    Phone1 = table.Column<string>(maxLength: 25, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 20, nullable: true),
                    Rating = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_person", x => x.Id);
                    table.ForeignKey(
                        name: "fk_PersonCountryId",
                        column: x => x.CountryId,
                        principalTable: "country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "event",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 1024, nullable: false),
                    Ends = table.Column<DateTime>(type: "datetime", nullable: true),
                    EventCategoryId = table.Column<int>(type: "int(11)", nullable: true),
                    EventTopicId = table.Column<int>(type: "int(11)", nullable: true),
                    Image = table.Column<string>(maxLength: 255, nullable: false),
                    Location = table.Column<string>(maxLength: 120, nullable: false),
                    Name = table.Column<string>(maxLength: 120, nullable: false),
                    OrganiserDescription = table.Column<string>(maxLength: 120, nullable: false),
                    OrganiserName = table.Column<string>(maxLength: 120, nullable: false),
                    Starts = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_event", x => x.Id);
                    table.ForeignKey(
                        name: "fk_EventEventCategoryId",
                        column: x => x.EventCategoryId,
                        principalTable: "eventcategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_EventEventTopicId",
                        column: x => x.EventTopicId,
                        principalTable: "eventtopic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    HashedPassword = table.Column<string>(maxLength: 255, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    PersonId = table.Column<int>(type: "int(11)", nullable: true),
                    RoleId = table.Column<int>(type: "int(11)", nullable: true),
                    Salt = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.Id);
                    table.ForeignKey(
                        name: "fk_UserPersonId",
                        column: x => x.PersonId,
                        principalTable: "person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_UserRoleId",
                        column: x => x.RoleId,
                        principalTable: "role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "uc_Country_Code",
                table: "country",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "uc_Country_Name",
                table: "country",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fk_EventEventCategoryId",
                table: "event",
                column: "EventCategoryId");

            migrationBuilder.CreateIndex(
                name: "fk_EventEventTopicId",
                table: "event",
                column: "EventTopicId");

            migrationBuilder.CreateIndex(
                name: "uc_EventCategory_Name",
                table: "eventcategory",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "uc_EventTopic_Name",
                table: "eventtopic",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fk_PersonCountryId",
                table: "person",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "uc_Role_Name",
                table: "role",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "uc_User_Name",
                table: "user",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fk_UserPersonId",
                table: "user",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "fk_UserRoleId",
                table: "user",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "event");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "eventcategory");

            migrationBuilder.DropTable(
                name: "eventtopic");

            migrationBuilder.DropTable(
                name: "person");

            migrationBuilder.DropTable(
                name: "role");

            migrationBuilder.DropTable(
                name: "country");
        }
    }
}
