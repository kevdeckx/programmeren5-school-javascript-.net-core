﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;

namespace FricFrac.Controllers
{
    public class PersonController : Controller
    {
        private docent1Context db = new docent1Context();
        public IActionResult Index()
        {
            List<Person> people = db.Person.ToList();
            ViewBag.People = people;
            return View();
        }
        public IActionResult Create()
        {
            List<Person> people = db.Person.ToList();
            List<Country> countries = db.Country.ToList();
            ViewBag.People = people;
            ViewBag.Countries = countries;
            return View();
        }
        [HttpPost]
        public IActionResult Create(Person person)
        {
            if (ModelState.IsValid)
            {
                db.Person.Add(person);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}
