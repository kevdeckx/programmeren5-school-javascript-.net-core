﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    [Table("user")]
    public partial class User
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Salt { get; set; }
        [StringLength(255)]
        public string HashedPassword { get; set; }
        [Column(TypeName = "int(11)")]
        public int? PersonId { get; set; }
        [Column(TypeName = "int(11)")]
        public int? RoleId { get; set; }
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [ForeignKey("PersonId")]
        [InverseProperty("User")]
        public Person Person { get; set; }
        [ForeignKey("RoleId")]
        [InverseProperty("User")]
        public Role Role { get; set; }
    }
}
