﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    [Table("person")]
    public partial class Person
    {
        public Person()
        {
            User = new HashSet<User>();
        }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(120)]
        public string LastName { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        [StringLength(255)]
        public string Password { get; set; }
        [StringLength(255)]
        public string Address1 { get; set; }
        [StringLength(255)]
        public string Address2 { get; set; }
        [StringLength(20)]
        public string PostalCode { get; set; }
        [StringLength(80)]
        public string City { get; set; }
        [Column(TypeName = "int(11)")]
        public int? CountryId { get; set; }
        [StringLength(25)]
        public string Phone1 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Birthday { get; set; }
        [Column(TypeName = "int(11)")]
        public int? Rating { get; set; }
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [ForeignKey("CountryId")]
        [InverseProperty("Person")]
        public Country Country { get; set; }
        [InverseProperty("Person")]
        public ICollection<User> User { get; set; }
    }
}
