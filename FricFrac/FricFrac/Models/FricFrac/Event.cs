﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    [Table("event")]
    public partial class Event
    {
        [Required]
        [StringLength(120)]
        public string Name { get; set; }
        [Required]
        [StringLength(120)]
        public string Location { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Starts { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Ends { get; set; }
        [Required]
        [StringLength(255)]
        public string Image { get; set; }
        [Required]
        [StringLength(1024)]
        public string Description { get; set; }
        [Required]
        [StringLength(120)]
        public string OrganiserName { get; set; }
        [Required]
        [StringLength(120)]
        public string OrganiserDescription { get; set; }
        [Column(TypeName = "int(11)")]
        public int? EventCategoryId { get; set; }
        [Column(TypeName = "int(11)")]
        public int? EventTopicId { get; set; }
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [ForeignKey("EventCategoryId")]
        [InverseProperty("Event")]
        public Eventcategory EventCategory { get; set; }
        [ForeignKey("EventTopicId")]
        [InverseProperty("Event")]
        public Eventtopic EventTopic { get; set; }
    }
}
