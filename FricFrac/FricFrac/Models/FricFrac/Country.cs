﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    [Table("country")]
    public partial class Country
    {
        public Country()
        {
            Person = new HashSet<Person>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(2)]
        public string Code { get; set; }
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
        [StringLength(256)]
        public string Desc { get; set; }
        [InverseProperty("Country")]
        public ICollection<Person> Person { get; set; }
    }
}
