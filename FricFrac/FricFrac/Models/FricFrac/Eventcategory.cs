﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    [Table("eventcategory")]
    public partial class Eventcategory
    {
        public Eventcategory()
        {
            Event = new HashSet<Event>();
        }

        [Required]
        [StringLength(120)]
        public string Name { get; set; }
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [InverseProperty("EventCategory")]
        public ICollection<Event> Event { get; set; }
    }
}
