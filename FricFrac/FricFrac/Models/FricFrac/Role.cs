﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    [Table("role")]
    public partial class Role
    {
        public Role()
        {
            User = new HashSet<User>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [InverseProperty("Role")]
        public ICollection<User> User { get; set; }
    }
}
