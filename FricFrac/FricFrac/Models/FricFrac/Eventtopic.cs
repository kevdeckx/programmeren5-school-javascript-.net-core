﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    [Table("eventtopic")]
    public partial class Eventtopic
    {
        public Eventtopic()
        {
            Event = new HashSet<Event>();
        }

        [Required]
        [StringLength(120)]
        public string Name { get; set; }
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [InverseProperty("EventTopic")]
        public ICollection<Event> Event { get; set; }
    }
}
