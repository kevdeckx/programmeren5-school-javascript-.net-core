﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FricFrac.Models.FricFrac
{
    public partial class docent1Context : DbContext
    {
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<Eventcategory> Eventcategory { get; set; }
        public virtual DbSet<Eventtopic> Eventtopic { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=127.0.0.1; user id =root;password=;port=3306;database=docent1;Sslmode=none");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasIndex(e => e.Code)
                    .HasName("uc_Country_Code")
                    .IsUnique();

                entity.HasIndex(e => e.Name)
                    .HasName("uc_Country_Name")
                    .IsUnique();
            });

            modelBuilder.Entity<Event>(entity =>
            {
                entity.HasIndex(e => e.EventCategoryId)
                    .HasName("fk_EventEventCategoryId");

                entity.HasIndex(e => e.EventTopicId)
                    .HasName("fk_EventEventTopicId");

                entity.HasOne(d => d.EventCategory)
                    .WithMany(p => p.Event)
                    .HasForeignKey(d => d.EventCategoryId)
                    .HasConstraintName("fk_EventEventCategoryId");

                entity.HasOne(d => d.EventTopic)
                    .WithMany(p => p.Event)
                    .HasForeignKey(d => d.EventTopicId)
                    .HasConstraintName("fk_EventEventTopicId");
            });

            modelBuilder.Entity<Eventcategory>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("uc_EventCategory_Name")
                    .IsUnique();
            });

            modelBuilder.Entity<Eventtopic>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("uc_EventTopic_Name")
                    .IsUnique();
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.HasIndex(e => e.CountryId)
                    .HasName("fk_PersonCountryId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Person)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("fk_PersonCountryId");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("uc_Role_Name")
                    .IsUnique();
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("uc_User_Name")
                    .IsUnique();

                entity.HasIndex(e => e.PersonId)
                    .HasName("fk_UserPersonId");

                entity.HasIndex(e => e.RoleId)
                    .HasName("fk_UserRoleId");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("fk_UserPersonId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("fk_UserRoleId");
            });
        }
    }
}
