function a() {
    alert('A!');
    return function() {
        alert('B!');
    };
}

function hackFunction(functionObject) {
    let text = functionObject.toString();
    alert(`functie in text: ${text}`);
    let newText = text.replace('B!', 'Gehack!');
    alert(newText);
    let newF = new Function(newText);
    newF();
}

function closureVoorbeeld1() {
    var something = 'hello';
    setTimeout(function() { console.log(something); }, 1000);
    var something = 'tot ziens';
}

function closureVoorbeeld2() {
    var something = 'hello';
    setTimeout(function() {
        console.log(`Zonder closure: ${something}`);
    }, 1000);

    setTimeout((function(something) {
        // hier wirdt een lokale copy
        // van gemaakt van de doorgegeven
        // parameter
        return function() {
            console.log(`Met closure: ${something}`);
        }
    })(something), 1000);
    something = 'tot ziens';

}

const arr = [1, 2, 30, 4];

function descending(a, b) {
    let comparison = 0;

    if (a > b) {
        comparison = -1;
    }
    else if (b > a) {
        comparison = 1;
    }

    return comparison;
}

function ascending(a, b) {
    let comparison = 0;

    if (a > b) {
        comparison = 1;
    }
    else if (b > a) {
        comparison = -1;
    }

    return comparison;
}


// => 1, 2, 4, 30

persons = [{
        Voornaam: 'Jan',
        Familienaam: 'Janssens'
    },
    {
        Voornaam: 'Mohamed',
        Familienaam: 'El Farisi'
    },
    {
        Voornaam: 'José',
        Familienaam: 'Dewachter'
    }
];

function compareLastName(a, b) {
    if (a.Familienaam > b.Familienaam) {
        comparison = 1;
    }
    else if (b.Familienaam > a.Familienaam) {
        comparison = -1;
    }
    return comparison;
}

function sorteerFamilienaam() {
    persons.sort(function(a, b) {
        if (a.Familienaam > b.Familienaam) {
            comparison = 1;
        }
        else if (b.Familienaam > a.Familienaam) {
            comparison = -1;
        }
        return comparison;
    })
}

function voorbeeldMap1() {
    return persons.map(function(person) {
        person.Familienaam = person.Familienaam.toUpperCase();
        return person;
    })
}

function sintMap() {
    return persons.map(function(person, i) {
        person.Familienaam = person.Familienaam.toUpperCase();
        person.Krijgt = i * 3;
        return person;
    })
}

function reduceVoorbeeld1 () {
    getallen = [98, 2, 2, 4, 56, 7, 4];
    console.log('som: ', getallen.reduce(function(total, item) {return total + item;}))
}
