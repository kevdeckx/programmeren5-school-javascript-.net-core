function loadJommeke() {
  let ajax;
  if (window.XMLHttpRequest) {
    ajax = new XMLHttpRequest();
  } else {
    // code for older browsers
    ajax = new ActiveXObject("Microsoft.XMLHTTP");
  }
  // callback functie
  ajax.onreadystatechange = function() {
      // server sent response and
      // OK 200 The request was fulfilled.
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("jommeke").innerHTML =
      this.responseText;
    }
  };
  ajax.open("GET", "data/jommeke.html", true);
  ajax.send();
}

function controller() {
    document.addEventListener('click',
        function(event) {
            // alert (event.target.tagName)
            if (event.target.tagName == 'BUTTON') {
                if (event.target.getAttribute('value') == 'jommeke') {
                    loadJommeke();
                } else if (event.target.getAttribute('value') == 'vos') {
                    $http('data/identitys.json')
                        .get()
                        .then(function () {
                            alert('You are a hero!');
                        })
                        .catch(function() {
                            alert('Sorry, no result...');
                        });
                }
            }
        }, false);
}

// controller laden bij het opstarten van het programma
window.onload = function () {
    controller();
}