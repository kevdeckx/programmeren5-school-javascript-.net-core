var vos = {
    model : {
        loaded: false,
        identity: {},
        procedureList: {},
        organisationList: {},
        position: {},
        myLocation: {}
    },
    setModel: function () {
        $http('data/identity.json')
        .get()
        .then(function (data) {
            vos.model.identity = JSON.parse(data);
            var payload = {};
            // procedures depend on Role (in uppercase)
            var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
            return $http(fileName).get(payload);
        })
        .then(function (data) {
            vos.model.procedureList = JSON.parse(data);
            // payload gevuld als voorbeeld
            var payload = {
                "firstName" : "Linda",
                "LastName" : "Trump"
            };
            var fileName = 'data/position.json';
            return $http(fileName).get(payload);
        })
        .then(function (data) {
                    vos.model.position = JSON.parse(data);
                    var payload = {};
                    return $http('data/organisationList.json').get(payload);
                })
                .then(function (data) {
                    vos.model.organisationList = JSON.parse(data);
                    vos.model.loaded = true;
                })
                .catch(function (data) {
                    vos.model.loaded = false;
                });
    },
    plopperdeplop: {
        
    }
}

vos.setModel();
