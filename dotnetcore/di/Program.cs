﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace DI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Dependency Injection leren gebruiken.");
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            // create service provider
            ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            serviceProvider.GetService<IDal>().TestConnector();
            DI.BLL.EventCategory eventCategory = new BLL.EventCategory(serviceProvider.GetService<IDal>());
            eventCategory.ReadAll();
            Console.ReadLine();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<DI.IDal>(p => new DI.DalTest());
        }
    }
}
