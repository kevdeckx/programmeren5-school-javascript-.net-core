﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DI.BLL
{
    class EventCategory
    {
        private readonly DI.IDal dal;
        public EventCategory(DI.IDal dal)
        {
            this.dal = dal;
        }
        public void ReadAll()
        {
            this.dal.TestConnector();
        }
    }
}
