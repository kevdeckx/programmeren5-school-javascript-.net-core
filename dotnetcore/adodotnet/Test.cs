﻿using System;
using MySql.Data.MySqlClient;
using System.Data;
using System.Text;

namespace AdoDotNet
{
    class Learning
    {
        private static string connectionString = "server=164.132.231.13;user id=docent1;password=UKSSL14H;port=3306;database=docent1;SslMode=none;";
        private static string database = "docent1";

        public static void TestMySqlConnector()
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            try
            {
                connection.Open();
                Console.WriteLine("Connectie met database gemaakt");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Connectie niet gemaakt met database, fout: {e.Message}");

            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connectie afgesloten");

            }
        }

        public static void CreateEventCategory()
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            try
            {
                connection.Open();
                StringBuilder sql = new StringBuilder();
                sql.Append("CREATE TABLE `EventCategory` (");
                sql.Append("`Name` NVARCHAR (120) NOT NULL,");
                sql.Append("`Id` INT NOT NULL AUTO_INCREMENT,");
                sql.Append("CONSTRAINT PRIMARY KEY(Id),");
                sql.Append("CONSTRAINT uc_EventCategory_Name UNIQUE (`Name`));");
                MySqlCommand command = new MySqlCommand(sql.ToString(), connection);
                command.ExecuteNonQuery();
                Console.WriteLine("Tabel EventCategory gemaakt");
            }
            catch (Exception e)
            {
                Console.WriteLine("Tabel niet gemaakt {0}", e.Message);
                Console.WriteLine($"Tabel niet gemaakt {e.Message}");
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connectie afgesloten");

            }
        }

        public static void CreateEventCategoryInsert()
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            try
            {
                connection.Open();
                StringBuilder sql = new StringBuilder();
                sql.AppendFormat("USE {0}\n", database);
                sql.AppendLine("DROP PROCEDURE IF EXISTS EventCategoryInsert;");
                sql.AppendLine("DELIMITER //");
                sql.AppendLine("CREATE PROCEDURE EventCategoryInsert (");
                sql.Append("IN `pName` NVARCHAR (120),");
                sql.AppendLine("OUT `pId` INT)");
                sql.AppendLine("BEGIN");
                sql.AppendLine("IF NOT EXISTS (SELECT * from EventCategory WHERE `Name` = pName )");
                sql.AppendLine("THEN");
                sql.AppendLine("INSERT INTO `EventCategory`(`EventCategory`.`Name`)");
                sql.AppendLine("VALUES ( pName);");
                sql.AppendLine("SELECT LAST_INSERT_ID() INTO pId;");
                sql.AppendLine("ELSE");
                sql.AppendLine("set pId = -100; -- Name exitst already");
                sql.AppendLine("END IF");
                sql.AppendLine("END //");
                sql.AppendLine("DELIMITER ;");
                Console.WriteLine(sql.ToString());
                MySqlCommand command = new MySqlCommand(sql.ToString(), connection);
                command.ExecuteNonQuery();
                Console.WriteLine("Stored procedure EventCategoryInsert gemaakt");
            }
            catch (Exception e)
            {
                Console.WriteLine("Stored procedure EventCategoryInsert niet gemaakt {0}", e.Message);
                Console.WriteLine($"Stored procedure EventCategoryInsert niet gemaakt {e.Message}");
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connectie afgesloten");

            }
        }

        public static void InsertEventCategory()
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            try
            {
                connection.Open();
                StringBuilder sql = new StringBuilder();
                sql.AppendFormat("USE {0};\n", database);
                sql.AppendLine("INSERT INTO `EventCategory`(`EventCategory`.`Name`)");
                sql.AppendLine("VALUES ('Appearance or Signing')");
                sql.AppendLine(", ('Attraction'), ");
                sql.AppendLine(", ('Camp.Trip or Retreat');");
                sql.AppendLine(", ('Concert or Performance');");
                sql.AppendLine(", ('Conference');");
                sql.AppendLine(", ('Convention');");
                sql.AppendLine(", ('Course, Training or Workshop');");
                sql.AppendLine(", ('Dinner or Gala');");
                sql.AppendLine(", ('Festival or Fair');");
                sql.AppendLine(", ('Game or Competition');");
                sql.AppendLine(", ('Meeting or Networking Event');");
                sql.AppendLine(", ('Other');");
                sql.AppendLine(", ('Party or Social Gathering');");
                sql.AppendLine(", ('Race or Endurance Event');");
                sql.AppendLine(", ('Rally');");
                sql.AppendLine(", ('Screening');");
                sql.AppendLine(", ('Seminar or Talk');");
                sql.AppendLine(", ('Tour');");
                sql.AppendLine(", ('Tournament');");
                sql.AppendLine(", ('Tradeshow, Consumer Show or Expo');");

                Console.WriteLine(sql.ToString());
                MySqlCommand command = new MySqlCommand(sql.ToString(), connection);
                command.ExecuteNonQuery();
                Console.WriteLine("EventCategory Insert geslaagd");
            }
            catch (Exception e)
            {
                Console.WriteLine("EventCategory Insert niet geslaagd {0}", e.Message);
                Console.WriteLine($"EventCategoryInsert niet geslaagd {e.Message}");
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connectie afgesloten");

            }
        }
    }
}
