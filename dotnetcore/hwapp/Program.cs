﻿using System;
﻿using System.Collections.Generic;

namespace hwapp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Dictionary<string, string> woordenboek = new Dictionary<string, string>();
            woordenboek.Add("Constructor", "Een methode die een exemplaar van een klasse maakt");
            woordenboek.Add("Operator", "Bijvoorbeeld een rekenkundige bewerking");
            foreach (KeyValuePair<string, string> woord in woordenboek)
            {
                Console.WriteLine($"Het woord {woord.Key} betekent {woord.Value}");
            }
            int[] getallen = new int[] { 3, 4, 5 }; // Declare int array
            int total = 0;
            foreach(int i in getallen)
            {
                total += i;
            }
            Console.WriteLine($"Het gemiddelde is {total / getallen.Length}");
            Console.ReadKey();
        }
    }
}
