// 'object' maken
// begint met hoofdletter omdat we die functie als
// een soort van klasse gaan gebruiken
function Persoon(voornaam, familienaam) {
    this.voornaam = voornaam;
    this.familienaam = familienaam;
    this.sayName = function () {
        alert(`Mijn naam is ${this.voornaam} ${this.familienaam}`);
    }
}

var jan = new Persoon('Jan', 'Jannsens');
var mohamed = new Persoon('Mohamed', 'El Farisi');
var mathilde = new Persoon('Mathilde', 'Deblock');

Persoon.prototype.wieBenIkNiet = function() {
    alert (`Mijn naam is ${this.voornaam} ${this.familienaam} en ik ben niet ....`);
}

jan.speciaal = function () {
    alert('Ik ben jan en de anderen heb dit niet!');
}