function TableSort(id) {
    // When calling an object constructor or any of its methods, 
    // this’ refers to the instance of the object
    // much like any class-based language
    this.tableElement = document.getElementById(id);
    if (this.tableElement && this.tableElement.nodeName == "TABLE") {
        this.prepare();
    }
}

TableSort.prototype.prepare = function() {
    // add arrow up
    // default is ascending order
    var headings = this.tableElement.tHead.rows[0].cells;
    // headings is een htmlcollection
    for (let i = 0; i < headings.length; i++) {
        headings[i].innerHTML = headings[i].innerHTML + '<span>&nbsp;&nbsp;&uarr;</span>';
        headings[i].className = 'asc';
    }
    // event listener toevoegen aan tabel
    // die luistert naar klikken op header kolommen
    this.tableElement.addEventListener("click", function(that) {
        // return that.eventHandler;
        return function() {
           that.eventHandler(event);
            that.eventHandler(arguments[0]);
        }
    }(this), false);
}

// de eventafhandelaar toevoegen
TableSort.prototype.eventHandler = function(event) {
    if (event.target.tagName === 'TH') {
        // alert('kolomkop');
        this.sortColumn(event.target);
    }
    else if (event.target.tagName === 'SPAN') {
        if (event.target.parentNode.tagName === 'TH') {
            if (event.target.parentNode.className == "asc") {
                event.target.parentNode.className = 'desc';
                event.target.innerHTML = "&nbsp;&nbsp;&darr;"
            }
            else {
                event.target.parentNode.className = 'asc';
                event.target.innerHTML = "&nbsp;&nbsp;&uarr;"
            };
        }
    }
}
// rijen sorteren op de inhoud van de geselecteerde kolom

// stop alle gegevens van de geselecteerde kolom in:
// - numerieke array als het getallen zijn
// - alfa array als het tekst is
// - hou een referentie bij naar de rij

// orden alfa en num  en voeg ze samen

// doorloop de geordende array en creëer geordende tabel
TableSort.prototype.sortColumn = function(headerCell) {
    // Get cell data for column that is to be sorted from HTML table
    let rows = this.tableElement.rows;
    let alpha = [],
        numeric = [];
    let alphaIndex = 0,
        numericIndex = 0;
    let cellIndex = headerCell.cellIndex;
    for (var i = 1; rows[i]; i++) {
        let cell = rows[i].cells[cellIndex];
        let content = cell.textContent ? cell.textContent : cell.innerText;
        let numericValue = content.replace(/(\$|\,|\s)/g, "");
        if (parseFloat(numericValue) == numericValue) {
            numeric[numericIndex++] = {
                value: Number(numericValue),
                row: rows[i]
            }
        }
        else {
            alpha[alphaIndex++] = {
                value: content,
                row: rows[i]
            }
        }

        let orderdedColumns = [];
        numeric.sort(function(a, b) {
            return a.value - b.value;
        });

        alpha.sort(function(a, b) {
            let aName = a.value.toLowerCase();
            let bName = b.value.toLowerCase();
            if (aName < bName) {
                return -1
            }
            else if (aName > bName) {
                return 1;
            }
            else {
                return 0;
            }
        });
        orderdedColumns = numeric.concat(alpha);
        let tBody = this.tableElement.tBodies[0];
        for (let i = 0; orderdedColumns[i]; i++) {
            tBody.appendChild(orderdedColumns[i].row);
        }
    }
}
