﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VosAPI.Models;

namespace VosAPI.Controllers
{
  
    public class AdminController : Controller
    {

        private LogContext _context;

        public AdminController(LogContext context)
        {
            _context = context;
        }

       [Route("Admin/Index")]
        public IActionResult Index()
        {
           return View(_context.Log.ToList());
        }

        [Route("Admin/Creating")]
        public IActionResult Creating()
        {
            return View();
        }

        [Route("Admin/Create")]
        [HttpPost]
        public IActionResult Create(Log log)
        {
            _context.Add(log);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }


        [Route("Admin/Editing/{id}")]
        public IActionResult Editing(int id)
        {
            var log = _context.Log.Find(id);
            return View(log);
        }

        [Route("Admin/Edit")]
        [HttpPost]
        public IActionResult Edit(Log log)
        {
            _context.Entry(log).State = EntityState.Modified;
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        [Route("Admin/Delete/{id}")]
        public IActionResult Delete(int id)
        {
            var log = _context.Log.Find(id);
            _context.Remove(log);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        [Route("Admin/Details/{id}")]
        public IActionResult Details(int id)
        {
            var log = _context.Log.Find(id);
            return View(log);
        }
    }
}