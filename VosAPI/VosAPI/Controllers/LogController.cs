﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using VosAPI.Models;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using System;

namespace VosAPI.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/vos")]
    public class LogController : Controller
    {
        private  LogContext _context;

        public LogController(LogContext context)
        {
            _context = context;

        }

        [HttpGet]
        public IEnumerable<Log> GetAll()
        {
            return _context.Log.ToList();
        }

        [HttpGet("{id}", Name = "GetLog")]
        public IActionResult GetById(long id)
        {
            var item = _context.Log.FirstOrDefault(l => l.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
        [HttpPost]
        public IActionResult Create([FromHeader] Log item)
        {
            if (item == null)
            {
                return BadRequest();
            }
            item.TimeStamp = DateTime.Now;
            _context.Log.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetLog", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Log item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            var log = _context.Log.FirstOrDefault(l => l.Id == id);
            if (log == null)
            {
                return NotFound();
            }

     
            log.UserName = item.UserName;

            _context.Log.Update(log);
            _context.SaveChanges();
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var log = _context.Log.FirstOrDefault(l => l.Id == id);
            if (log == null)
            {
                return NotFound();
            }

            _context.Log.Remove(log);
            _context.SaveChanges();
            return new NoContentResult();
        }

    }
}