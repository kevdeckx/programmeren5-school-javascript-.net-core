﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using VosAPI.Models;
using Microsoft.AspNetCore.Cors;

namespace VosAPI
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            //vervang dit met mysql
            // services.AddDbContext<LogContext>(opt => opt.UseInMemoryDatabase("Log"));
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddDbContext<LogContext>(opt => opt.UseMySQL("server=127.0.0.1; user id=kevindeckx;password=;port=3306;database=vosapi;Sslmode=none"));
            services.AddMvc();
   
            
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseMvc();
        }
    }
}