use FricFrac;
DROP PROCEDURE IF EXISTS EventCategoryInsert;
DELIMITER //
CREATE PROCEDURE `EventCategoryInsert`
(     IN pName NVARCHAR (120) ,
    OUT pId INT
)
BEGIN
    IF NOT EXISTS (SELECT * from EventCategory WHERE `Name` = pName )
    THEN
        INSERT INTO `EventCategory`
        (
            `EventCategory`.`Name`
        )
        VALUES (
            pName
        );
        -- return the Id of the inserted row
        SELECT LAST_INSERT_ID() INTO pId;
    else
        set pId = -100; -- Name exitst already
    END IF;

END //
DELIMITER ;

DROP PROCEDURE IF EXISTS EventCategoryUpdate;
DELIMITER //
CREATE PROCEDURE `EventCategoryUpdate`
(
	pName NVARCHAR (120) ,
	pId INT 
)
BEGIN
UPDATE `EventCategory`
	SET
		`Name` = pName
	WHERE `EventCategory`.`Id` = pId;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS EventCategoryDelete;
DELIMITER //
CREATE PROCEDURE `EventCategoryDelete`
(
	 pId INT 
)
BEGIN
DELETE FROM `EventCategory`
	WHERE `EventCategory`.`Id` = pId;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS EventInsert;
DELIMITER //
CREATE PROCEDURE `EventInsert`
(     IN pName NVARCHAR (120) ,
      IN pLocation nvarchar(120),
      IN pStarts datetime,
      IN pEnds datetime,
      IN pImage nvarchar(255),
      IN pDescription nvarchar(1024),
      IN pOrganiserName nvarchar(120),
      IN pOrganiserDescription(120),
      IN pEventCategoryId int,
      IN pEventTopicId int,
    OUT pId INT
)
BEGIN
    IF NOT EXISTS (SELECT * from Event WHERE `Name` = pName )
    THEN
        INSERT INTO `Event`
        (
            `Event`.`Name`,
            `Event`.`Location`,
            `Event`.`Starts`,
            `Event`.`Ends`,
            `Event`.`Image`,
            `Event`.`Description`,
            `Event`.`OrganiserName`,
            `Event`.`OrganiserDescription`,
            `Event`.`EventCategoryId`,
            `Event`.`EventTopicId`,
        )
        VALUES (
            pName,
            pLocation,
            pStarts,
            pEnds,
            pImage,
            pDescription,
            pOrganiserName,
            pOrganiserDescription,
            pEventCategoryId,
            pEventTopicId,
        );
        -- return the Id of the inserted row
        SELECT LAST_INSERT_ID() INTO pId;
    else
        set pId = -100; -- Name exitst already
    END IF;

END //
DELIMITER ;