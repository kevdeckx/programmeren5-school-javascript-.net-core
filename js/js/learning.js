window.onload = function () {
    let buttonLabelColor = document.getElementById('changeLabelColor');
    // traditioneel event model
    // buttonLabelColor.onclick = changeLabelColor();
    buttonLabelColor.addEventListener('click', changeLabelColor, false);
}

function lerenWerkenMetLet() {
    let y = 3.5;
    let x = 4;
    let firstName = 'Samantha';
    let lastName = 'Brooks';
}

function werkenMetArrays() {
    let punten = new Array(5);
    alert(punten.length);
    for (let i = 0; i < punten.length; i++) {
        alert(punten[i]);
    }
    let nogPunten = new Array(10, 12, 0, 6, 9, 7);
    alert(nogPunten.length);
    // nogPunten[100] = 2;
    nogPunten['andre'] = 2;
    for (let i = 0; i < nogPunten.length; i++) {
        alert(nogPunten[i]);
    }

    alert(`De punten van andre zijn: ${nogPunten['andre']}`);

    delete nogPunten['andre'];
    alert(`De lengte van nogPunten is: ${nogPunten.length}`);


}

function functiesZijnData() {
    var x = 20;
    let zegHet = function () {
        alert('Ik heb het gezegd.');
    }
    alert(`In de functievariabele staat: ${zegHet}`);
    zegHet();
}

function changeLabelColor() {
    let labels = document.getElementsByTagName('label');
    alert(`Het aantal labels op de form: ${labels.length}`);
    for (let i = 0; i < labels.length; i++) {
        // labels[i].style.color = 'green';
        changeOneLabelColor(labels[i]);
    }
}

let changeOneLabelColor = function (label) {
    label.style.color = 'green';
}

function changeLabelsMetForEach() {
    // let labels = document.getElementsByTagName('label');
    // const labels = document.querySelectorAll('label');
    // const labels = document.querySelectorAll('input[type="radio"]+label');
    const labels = document.querySelectorAll('label+input[type="text"]');
    
    alert(`Het aantal labels op de form: ${labels.length}`);
    // https://pawelgrzybek.com/loop-through-a-collection-of-dom-elements/
    // labels.forEach(changeOneLabelColor(item));
    // The ECMAScript 2015 spec brought us a new tool to traverse through 
    // iterable objects. As we saw in the previous example, 
    // NodeList is definitely an iterable collection so we can easily 
    // add a for..of loop to our collection. 
    // Babel may be helpful in this instance as it is a part of the spec 
    // that is a bit more modern than your clients requirements.
     
    for (let item of items) {
      item.previousElementSibling.style.color = 'red';
    }
    
}
