﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VosAPI.Models
{

    public class Log
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string UserName { get; set; }
        [Required]
        [StringLength(50)]
        public string Email { get; set; }
        [Required]
        [StringLength(255)]
        public string Role { get; set; }
        [Required]
        [StringLength(25)]
        public string ProcedureCode { get; set; }
        [Required]
        [StringLength(255)]
        public string ProcedureTitle { get; set; }
        [Required]
        [StringLength(255)]
        public string StepTitle { get; set; }
        [Required]
        [StringLength(25)]
        public string CallNumber { get; set; }
        [Required]
        [StringLength(25)]
        public string SendNumber { get; set; }
        [Required]
        public DateTime TimeStamp { get; set; }

    }
}
