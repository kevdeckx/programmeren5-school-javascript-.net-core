﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VosAPI.Migrations
{
    public partial class EditedLogModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsComplete",
                table: "Log");

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeStamp",
                table: "Log",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeStamp",
                table: "Log");

            migrationBuilder.AddColumn<bool>(
                name: "IsComplete",
                table: "Log",
                nullable: false,
                defaultValue: false);
        }
    }
}
